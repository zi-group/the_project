<?php

use Michelf\Markdown;

function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return trim(Markdown::defaultTransform($string_markdown_formatted));
}

function getEquipments()
{
    $response = Requests::get('http://www.dnd5eapi.co/api/equipment');
    return json_decode($response->body);
}

function getEquipmentDetail($index)
{
    $index = (int) $index;
    $response = Requests::get("http://www.dnd5eapi.co/api/equipment/$index");
    return json_decode($response->body);

}

function sayHello()
{
    $hello = "Hello World";
    echo ($hello);
}

function getDwarfs()
{
    $dwarfs = Dwarf::find_many();
    $result = array();
    foreach ($dwarfs as $dwarf) {
        $results[] = array(
            'name' => $dwarf->NAME,
            'height' => $dwarf->HEIGHT,
            'beardcolor' => $dwarf->BEARDCOLOR,
            'beardsize' => $dwarf->BEARDSIZE,
            'equipment' => $dwarf->EQUIPMENT,
            'company_id' => $dwarf->COMPANY_ID,
        );
    }
    return $results;
}

function getCompanies()
{
    $companies = Company::find_many();
    $result = array();
    foreach ($companies as $company) {
        $results[] = array(
            'name' => $company->NAME,
            'creator' => $company->CREATOR_ID,
        );
    }
    return $results;
}

function countElements($table)
{
    $count = $table::count();
    return $count;
}