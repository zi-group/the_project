<?php

class DWARF extends Model {
    public static $_table = "DWARF";
    public static $_id_column = 'ID';

    public function company()
    {
        return $this->belongs_to('company', 'COMPANY_ID')->find_one();
    }
}

class company extends model{
    public static $_table = 'company';
    public static $_id_column = 'ID';
    public function user()
    {
        return $this->belongs_to('user', 'CREATOR_ID')->find_one();
    }
}

class user extends model{
    public static $_table = 'user';
}