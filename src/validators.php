<?php

use \Tamtamchik\simpleflash\Flash;
use Respect\Validation\Validator as v;

function validateCompany($company)
{
    if((is_null($company->NAME)) or (strlen($company->NAME) < 5)){
        return false;
    }
    return true;
}

function validateDwarf($dwarf)
{
    if((is_null($dwarf->NAME)) or (strlen($dwarf->NAME) < 2) or ($dwarf->HEIGHT < 100)){
        echo $dwarf->NAME;
        echo $dwarf->HEIGHT;
        return false;
    }
    return true;
}

function validateUser($user)
{
    if((is_null($user->NAME) or (strlen($user->NAME) < 3)))
    {
        echo "Erreur dans le nom d'utilisateur";
        return false;
    }
    if((is_null($user->PASSWORD) or (strlen($user->PASSWORD) < 6)))
    {
        echo "Mot de passe trop court";
        return false;
    }
    return true;
}