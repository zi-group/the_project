<?php require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase
{

    public function testCountElements()
    {
        ORM::configure('sqlite:company.sqlite3');
        $this->assertEquals(3, countElements('company'));
    }

    public function testgetDwarfs()
    {
        ORM::configure('sqlite:company.sqlite3');
        $table = getDwarfs();
    }
    public function testgetEquipments()
    {
        $table = getEquipments();
        $this->assertEquals(256, $table->count);
    }
    public function test_renderHTMLFromMarkdown()
    {
        $this->assertEquals("<h2>Test</h2>", renderHTMLFromMarkdown("## Test"));
    }

    public function test_getEquipments()
    {
        $table = getEquipments();
        $this->assertEquals(256, $table->count);
    }

    public function test_getEquipmentDetail()
    {
        $detail = getEquipmentDetail(1);
        $this->assertEquals("Club", $detail->name);
    }

    public function test_sayHello()
    {
        $this->assertEquals("Hello World", sayHello());
    }

}