<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest
{

    public function test_indexGet()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Généranain", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_Equipments()
    {
        $response = $this->make_request("POST", "/equipments");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("256", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
    public function test_Dwarfs()
    {
        $response = $this->make_request("GET", "/dwarfs");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Thorin", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
    public function test_Companies()
    {
        $response = $this->make_request("GET", "/company");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Thorin and Company", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
    public function test_CompanyDetail()
    {
        $response = $this->make_request("POST", "/company/details/0");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Thorin and Company", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
}
