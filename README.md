Projet PHP de fin de matière WEB en DUT Informatique AS :

Université : Université de Picardie Jules Verne

Etablissement : IUT Amiens

Professeur : Samy Delahaye 

Référence Pr : https://www.delahayeyourself.info/

Prérequis : 

PHP version : 7.3.1

"mikecao/flight": "^1.3",

"rmccue/requests": "^1.7",

"phpunit/phpunit": "^6.5",

"guzzlehttp/guzzle": "^6.3",

"symfony/process": "^4.2",

"twig/twig": "^2.6",

"michelf/php-markdown": "^1.8",

"j4mie/paris": "^1.5",

"tamtamchik/simple-flash": "^1.2"

Style provenant essentiellement du framework CSS Bootstrap

Serveur : localhost:8080

Description:

Ce site correspond à notre projet de fin module web, il contient les features suivantes :

- Récupération d'une API et affichage des données reccueillies
- Création d'une entité "Compagnie" dans la base de données liée au site à partir d'un formulaire
- Création et modification d'une entité "Nain" à partir d'un formulaire puis 
enregistrement en base de données
- Récupération de l'ensemble des entités "Nain" et "Compagnie" pour permettre 
un affichage
- Création d'un compte utilisateur qui s'écrira dans la base de données avec mot de passe chiffré
- Interface de connexion utilisateur qui permet d'accéder à un profil basique
- A partir du profil utilisateur, ajout possible de l'ID utilisateur à une compagnie par le champs "CREATOR_ID" pour permettre un affichage spécifique des compagnies.
- Envoie de l'ensemble des entités "Nain" et "Compagnie" sous forme d'API



Auteurs : Arnaud ANDRE, Nicolas JORY, Raphaël SCELLIER