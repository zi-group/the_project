<?php require "vendor/autoload.php";

session_start();

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function ($string) {
        return renderHTMLFromMarkdown($string);
    }));
    $twig->addGlobal('session', $_SESSION);
    $twig->addFunction(new Twig_Function('messages', function () {
        return Flash::display();
    }));
});

Flight::before('start', function (&$params, &$output) {
    ORM::configure('sqlite:company.sqlite3');
});

Flight::map('render', function ($template, $data = array()) {
    Flight::view()->display($template, $data);
});

Flight::route('/first_view/', function () {
    $data = [
        'contenu' => 'Lorem ipsum',
    ];
    Flight::render('first_view.twig', $data);
});

Flight::route('/equipments/', function () {
    $data = [
        'equipments' => getEquipments(),
    ];
    Flight::render('equipments/equipments.twig', $data);
});

Flight::route('/', function () {
    if(!isset($_SESSION['logged']))
    {
        $_SESSION = false;
    }  
    Flight::render('index.twig');
});

Flight::route('/faq/', function () {
    Flight::render('faq.twig');
});

Flight::route('/about/', function () {
    Flight::render('about.twig');
});

Flight::route('/aboutus/', function () {
    Flight::render('aboutus.twig');
});

Flight::route('/equipments/@index', function ($index) {
    $data = [
        'equipment' => getEquipmentDetail($index),
    ];
    Flight::render('equipments/equipment.twig', $data);
});

Flight::route('/dwarfs/', function () {
    $dwarfs = Model::factory('dwarf')->find_many();
    $data = [
        'dwarfs' => $dwarfs,
        'companies' => Model::factory('company')->find_many(),
    ];
    Flight::render('dwarfs/dwarfs.twig', $data);
});

Flight::route('/company', function () {
    $companies = Model::factory('company')->find_many();
    {
        $data = [
            'companies' => $companies,
        ];
    }
    Flight::render('companies/companies.twig', $data);
});

Flight::route('/company/detail/@id', function ($id) {
    $count = countElements("company");
    if (($id <= $count - 1) && ($id >= 0)) {
        $company = Model::factory('company')
            ->where('ID', $id)
            ->find_one();
        $dwarfs = Model::factory('dwarf')
            ->where('COMPANY_ID', $id)
            ->find_many();
        $data = [
            'company' => $company,
            'dwarfs' => $dwarfs,
        ];
        Flight::render('companies/detail.twig', $data);
    } else {
        Flight::redirect('/company');
    }

});

Flight::route('/company/form(/@id)', function ($id) {

    if (isset($id)) {
        $company = Model::factory('company')
            ->where('ID', $id)
            ->find_one();
    } else {
        $company = Model::factory('company')->create();
        $company->NAME = null;
    }

    if (Flight::request()->method == 'POST') {
        $company->NAME = Flight::request()->data->name;
        if (validateCompany($company)) {
            $company->save();
            Flight::redirect('/company');
        } else {
            $data = [
                'company' => $company,
            ];
            Flight::render('companies/formCompany.twig', $data);
        }
    } else {
        $data = [
            'company' => $company,
        ];
        Flight::render('companies/formCompany.twig', $data);
    };
});

Flight::route('/dwarfs/form(/@id)', function ($id) {
    if (isset($id)) {
        $dwarf = Model::factory('dwarf')
            ->where('ID', $id)
            ->find_one();
    } else {
        $dwarf = Model::factory('dwarf')->create();
        $dwarf->NAME = null;
        $dwarf->HEIGHT = null;
        $dwarf->BEARDSIZE = null;
        $dwarf->BEARDCOLOR = null;
        $dwarf->EQUIPMENT = null;
        $dwarf->COMPANY_ID = null;
    }
    if (Flight::request()->method == 'POST') {
        $dwarf->NAME = Flight::request()->data->name;
        $dwarf->HEIGHT = Flight::request()->data->height;
        $dwarf->BEARDCOLOR = Flight::request()->data->beardcolor;
        $dwarf->BEARDSIZE = Flight::request()->data->beardsize;
        $dwarf->EQUIPMENT = Flight::request()->data->equipment;
        $dwarf->COMPANY_ID = Flight::request()->data->company;
        if (validateDwarf($dwarf)) {
            $dwarf->save();
            Flight::redirect("/company/detail/$dwarf->COMPANY_ID");
        } else {
            $data = [
                'dwarf' => $dwarf,
                'equipments' => getEquipments(),
                'companies' => Model::factory('company')->find_many(),
            ];
            Flight::render('dwarfs/formDwarf.twig', $data);
        }
    } else {
        $data = [
            'dwarf' => $dwarf,
            'equipments' => getEquipments(),
            'companies' => Model::factory('company')->find_many(),
        ];
        Flight::render('dwarfs/formDwarf.twig', $data);
    };
});

Flight::route('/dwarfs/delete/@compID/@dwarfID', function ($compID, $dwarfID) {
    $dwarf = Model::factory('dwarf')
        ->where('ID', $dwarfID)
        ->find_one();
    $dwarf->delete();
    Flight::redirect("/company/detail/$compID");
});

Flight::route('/login', function () {
    if ((Flight::request()->method == 'POST') and ((Flight::request()->data->password)!= null)) 
    {
        $name = Flight::request()->data->name;
        $password = Flight::request()->data->password;
        $user = Model::factory('user')
            ->where('name', $name)
            ->find_one();
        if (password_verify($password, $user->PASSWORD)) {
            $_SESSION['logged'] = true;
            $_SESSION['name'] = $name;
            Flight::redirect("/profil/$name");
        } else {
            $data = [
                'erreur' => true,
            ];
            Flight::render('/users/login.twig', $data);
        }

    }
    else
    {
        Flight::render('/users/login.twig');
    }
});

Flight::route('/register', function () {
    if (Flight::request()->method == 'POST') {
        $user = Model::factory('user')->create();
        $user->NAME = Flight::request()->data->name;
        $user->PASSWORD = password_hash(Flight::request()->data->password, PASSWORD_DEFAULT);
        $name = Model::factory('user')
            ->where('name', $user->NAME)
            ->find_one();
        if (empty($name)) {
            
            if (validateUser($user)) {
                $user->save();
                Flight::redirect('/');
            } else {
                $data = [
                    'erreurFORM' => true,
                ];
                Flight::render('/users/register.twig', $data);
            }
        }
        else
        {
            $data=[
                'erreurUSER' => true,
            ];
            Flight::render('/users/register.twig', $data);
        }

    }
    Flight::render('/users/register.twig');
});

Flight::route('/disconnect',function()
    {
        $_SESSION['logged'] = false;
        $_SESSION['name'] = NULL;
        Flight::redirect('/');
    }
);

Flight::route('/profil/@username',function($username)
{
    if ($_SESSION['logged'])
    {
        $user = Model::factory('user')
        -> where('name', $username )
        ->find_one();
        $companies = Model::factory('company')
        -> where('CREATOR_ID', $user->ID)
        -> find_many();
        $data = [
            'user' => $user,
            'companies' => $companies,
        ];
        Flight::render('/users/mycompanies.twig',$data);
    }
    else
    {
        Flight::redirect('/');
    }
});

Flight::route('/profil/@username/companies',function($username){

    if ($_SESSION['logged'])
    {
        $user = Model::factory('user')
                -> where('name', $username )
                ->find_one();
        if(Flight::request()->method == 'POST')
        {
            $companyID = Flight::request()->data->company;
            $company = Model::factory('company')
                -> where('ID', $companyID)
                -> find_one();
            $company->CREATOR_ID = (int)$user->ID;
            $company->save();
            Flight::redirect("/profil/$username");
            

        }
        else
        {            
            $companies = Model::factory('company')->find_many();
            $data = [
                'user' => $user,
                'companies' => $companies, 
            ];
            Flight::render('/users/addPersonalCompany.twig',$data);
        }
    } 
    else
    {
        Flight::redirect('/');
    }
});

Flight::route('/api/companies', function(){
    $companies = Model::factory('company')->find_many();
    $data = [
        'companies' => getCompanies(),
    ];
    Flight::json($data);
});

Flight::route('/api/dwarfs', function(){
    $data = [
        'dwarfs' => getDwarfs(),
    ];
    Flight::json($data);
});


Flight::route('/error', function () {
    Flight::render('error.twig');
});

Flight::route('/test/', function () {

});

Flight::start();
